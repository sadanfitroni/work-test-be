const express = require('express')
const router = express.Router()
const User = require('../models/User')

router.get('/', (req, res) => {
    res.send('Hi')
})

// @desc            // get all users 
// @route           // GET 
router.get('/api/users/get_user', async (req, res) => {
    let user_data; 
    try {
        await User.find().then(data => {
            user_data = data
        })
        res.status(200).json(user_data) 
    } catch (error) {
        console.log(error)
        res.status(500).send('data not found')
    }
})

// @desc            // findOne document users.  
// @route           // GET 
router.get('/api/users/:_id', async (req, res) => {
    try {
        await User.findOne({_id : req.params._id}).then(data => {
            res.status(200).json(data)    
        })
         
    } catch (error) {
        res.status(500).send('data not found')
    }
})
module.exports = router