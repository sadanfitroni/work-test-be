const express = require('express')
const dotenv = require('dotenv')
const connectDB = require('./config/db')

// load config
dotenv.config({path : './config/config.env'})
connectDB()
const app = express()

// Routes 
app.use('/', require('./routes/users'))

const PORT = process.env.PORT || 3003
app.listen(PORT, console.log(`server running on port ${PORT}` ))
